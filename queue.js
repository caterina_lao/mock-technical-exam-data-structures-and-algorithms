let collection = [];

// Write the queue functions below.

// Output all the elements of the queue
const print = () => {
      // for(let i = 0; i<collection.length; i++) {
      // console.log(collection);  
      return collection
      // } 	
}

// Adds element to the rear of the queue
const enqueue = () => {

   	collection.push('John');
   	return collection;
 	collection.push('Jane');
 	return collection
   	collection.push('Bob');
   	return collection
    collection.push('Cherry');
   	return collection
   
}


// Removes element from the front of the queue
const dequeue =(queue)=> {
	collection.shift();
  	return collection;
  
}

// Show element at the front
const front = (queue) => {
  return collection[0]; 
}

// Show total number of elements
const size = (queue) =>{
   return collection.length
}

// Outputs a Boolean value describing whether queue is empty or not
const isEmpty =(queue) => {
	if(collection.length == 0){
		return true     
	} else {
		return false
	}
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
